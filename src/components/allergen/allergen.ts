import {Component, Input} from '@angular/core';
import {UserProvider} from "../../providers/user";
import {ActionSheetController} from "ionic-angular";

@Component({
  selector: 'allergen',
  templateUrl: 'allergen.html'
})
export class AllergenComponent {

  _name: string;
  _text: string;
  _color?: 'danger' | 'caution';
  _readonly: boolean = true;


  @Input('toggler') set toggle (value:any) {
    this._readonly = false;
  };

  @Input('name') set init (name: string) {
    this._name = name;
    this.text = name;
    if (this.user.tailoring == 2)
      this._color = this.user.allergens[name];
  }

  @Input() large;

  set text (name: string) {
    const text: {[name: string]: string} = {
      eggs: 'Eier',
      gluten: 'Gluten',
      milk: 'Milch',
      soy: 'Soja',
      treeNuts: 'Nüsse',
      peanuts: 'Erdnüsse',
      sulphites: 'Sulfite',
      mustard: 'Senf',
      cellery: 'Sellerie',
      crustacean: 'Krustentiere',
      fish: 'Fisch',
      lupin: 'Lupine',
      molluscs: 'Weichtiere',
      sesameSeeds: 'Sesam'
    };
    this._text = text[name];
  }

  constructor(
    private user: UserProvider,
    private actionSheet: ActionSheetController
  ) {}

  changeValue () {
    return this.actionSheet.create({
      buttons: [
        {
          text: 'Heavy Allergy',
          cssClass: 'danger',
          handler: () => this.setAllergen('danger')
        },
        {
          text: 'Light Allergy',
          cssClass: 'caution',
          handler: () => this.setAllergen('caution')
        },
        {
          text: 'No Allergy',
          handler: () => this.setAllergen()
        },
      ]
    }).present()
  }

  setAllergen (value?: 'caution' | 'danger') {
    this.user.allergens[this._name] = value; // TODO Refactor to user or store here
    this._color = value;
  }


}
