import { NgModule } from '@angular/core';
import { AllergenComponent } from './allergen/allergen';
import {IonicModule} from "ionic-angular";
@NgModule({
	declarations: [AllergenComponent],
	imports: [IonicModule],
	exports: [AllergenComponent]
})
export class ComponentsModule {}
