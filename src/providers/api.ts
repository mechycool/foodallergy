import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ApiProvider {

  root = 'https://eatfit-service.foodcoa.ch';
  username = 'eatfit_student_michael';
  password = 'cbbw9$4TS4qxXaU#1XDuHYW5';

  constructor(public http: HttpClient) {}

  fetchProduct(gtin: number): Promise<Product> {
    return this.http.get(
      `${this.root}/products/${gtin}/?format=json`,
      {headers: new HttpHeaders().set('Authorization', 'Basic ' + btoa(`${this.username}:${this.password}`))}
    ).toPromise()
      .then((response: {products: ProductData[], success: boolean}) => {
        if (!response.success)
          throw 'notFound';
        // TODO API: respond with product instead of array of products, I only need one of them
        const data = response.products[0];
        const product: Product = {
          gtin: data.gtin,
          title: data.product_name_de, // Result is unilingual, for research purposes
          allergens: data.allergens.map(obj => {
            // Fixing ugly data structure from API TODO API: enable a nicer data structure
            const allergen = obj.name.replace('allergen','');
            return allergen.substring(0,1).toLowerCase() + allergen.substring(1);
          })
        };
        if (data.image)
          product.img = `${this.root}/${data.image}`;
        return product;

      })
      /*.catch(err => {
        // TODO Find out how this could happen;
        console.log(err);
        throw 'requestFailed';
      })*/
  }

}

//

/*
  return this.alert.create({
    title: 'Product not found', // TODO i18n
    buttons: [{text: 'Nevermind'}]
  }).present();
}
*/


interface ProductData {
  allergens: {name: string}[];
  gtin: number;
  image: string;
  product_name_de: string;
}


export interface Product {
  gtin: number; // TODO or is it string?
  title: string;
  allergens: string[];
  img?: string;
}
