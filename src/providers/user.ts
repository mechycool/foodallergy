import { Injectable } from '@angular/core';
import {AlertController, LoadingController, ModalController} from "ionic-angular";
import {Storage} from "@ionic/storage";
import {AngularFirestore} from "angularfire2/firestore";
import {DocumentReference} from "@firebase/firestore-types";

@Injectable()
export class UserProvider {
  ref: DocumentReference|boolean = true; // TODO Remove value and declaration in production
  allergens:Allergens = {eggs: 'caution', gluten: 'danger'}; // TODO Remove value in production
  set testGroup (data:TestGroup) {
    this.tailoring = data.tailoring;
    this.display = data.display;
  }

  get testGroup () {
    return {
      tailoring: this.tailoring,
      display: this.display
    }
  }
  tailoring: 0|1|2 = 0;
  display: 'icon' |'text' = 'text';

  constructor(
    private storage: Storage,
    private loadingCtrl: LoadingController,
    private modal: ModalController,
    private db: AngularFirestore,
    private alert: AlertController,
  ) {}

  ready() {
    const loading = this.loadingCtrl.create();
    loading.present()
      .then(() => this.getFromStorage())
      .catch(() => this.createUser())
      .then(() => loading.dismiss())
  }

  getFromStorage(): Promise<DocumentReference> {
    return this.storage.get('userId')
      .then((userId:string) => {
        if (!userId)
          return null;
        return this.db.doc(`users/${userId}`).ref.get()
          .then(snap => {
            if (!snap.exists)
              return null;
            this.testGroup = snap.data().testGroup as TestGroup;
            this.allergens = snap.data().allergens as Allergens;
            return this.ref = snap.ref;
          })
      })
  }

  createUser(): Promise<any> {
    this.testGroup = {
      display: Math.floor(2*Math.random()) ? 'icon' : 'text',
      tailoring:<0|1|2> Math.floor(3*Math.random())
    };
    const modal = this.modal.create('SignUpPage', {}, {enableBackdropDismiss: false});
    const listener = new Promise(resolve => {
      modal.onWillDismiss((ref:DocumentReference) => {
        this.ref = ref;
        this.storage.set('userId',ref.id)
          .then(() => this.signedUpAlert())
          .then(() => resolve())
      });
    });
    return modal.present()
      .then(() => listener);
  }

  signedUpAlert() {
    return new Promise(resolve => {
      return this.alert.create({
        title: 'Thanks a lot',
        message: "Now you're ready to get started",
        buttons: [{
          text: "Let's Go",
          handler: () => resolve()
        }],
        enableBackdropDismiss: false
      }).present()
    })
  }

}

export interface TestGroup {
  display: 'icon'|'text';
  tailoring: 0|1|2;
}

interface Allergens {
  [name:string]: 'caution'|'danger';
}
