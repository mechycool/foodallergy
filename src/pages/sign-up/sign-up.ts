import { Component, ViewChild} from '@angular/core';
import {IonicPage, LoadingController, Slides, ViewController} from 'ionic-angular';
import { UserProvider} from "../../providers/user";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AngularFirestore} from "angularfire2/firestore";
import {Subscription} from "rxjs/Subscription";

@IonicPage()
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUpPage {

  title: string;
  allergens: string[] = [
    "eggs",
    "gluten",
    "milk",
    "soy",
    "treeNuts",
    "peanuts",
    "sulphites",
    "mustard",
    "cellery",
    "crustacean",
    "fish",
    "lupin",
    "molluscs",
    "sesameSeeds"
  ];



  activeView: string;
  views: string[];
  slideListener: Subscription;

  @ViewChild(Slides) slides: Slides;

  mainForm:FormGroup = new FormGroup({
    sex: new FormControl(null, Validators.required),
    birthYear: new FormControl(new Date('1986').toISOString(), [Validators.required,Validators.min(1900), Validators.max(2018)]),
    weight: new FormControl(null, [Validators.required, Validators.min(1),Validators.max(240)]),
    height: new FormControl(null, [Validators.required, Validators.min(1),Validators.max(500)]),
    dietFocus: new FormControl(null,Validators.required),
    dietType: new FormControl(null,Validators.required),
    german: new FormControl(null,Validators.required)
  });


  activityForm: FormGroup = new FormGroup({
    work: new FormControl(null,Validators.required),
    leisure: new FormControl(null,Validators.required),
  });

  miscForm: FormGroup = new FormGroup({
    dietFocus: new FormControl(null,Validators.required),
    dietType: new FormControl(null,Validators.required),
    german: new FormControl(null,Validators.required)
  });

  constructor(
    private user: UserProvider,
    private db: AngularFirestore,
    private viewCtrl: ViewController,
    private loading: LoadingController,
  ) {
    this.views = ['allergens','mainForm','activity'];
    this.setView();
  }

  ionViewDidLoad() {
    this.slideListener = this.slides.ionSlideWillChange
      .subscribe(data => this.setView(data.getActiveIndex()));
    this.slides.lockSwipeToNext(true);
  }

  ionViewWillLeave() {
    this.slideListener.unsubscribe();
  }

  setView(index:number = 0) {
    this.activeView = this.views[index];
     switch (this.activeView) {
      case 'allergens':
        this.title = 'Your Allergies';
        break;
      case 'mainForm':
        this.title = 'About You';
        break;
      case 'activity':
        this.title = 'Your Activities';
        break;
    }
  }

  next() {
    this.slides.lockSwipeToNext(false);
    this.slides.slideNext();
    this.slides.lockSwipeToNext(true);
  }

  setActivity(prop:'work'|'leisure',value: 0|1|2|3) {
    this.activityForm.get(prop).setValue(value);
    if (this.activityForm.valid)
      return this.submit();
    return this.next();
  }

  submit() {
    this.mainForm.value.birthYear = parseInt(this.mainForm.value.birthYear.substring(4));
    const meta = {
      ...this.mainForm.value,
      activity: this.activityForm.value,
    };
    const user = {
      testGroup: this.user.testGroup,
      allergens: this.user.allergens,
      meta
    };
    const loading = this.loading.create();
    return loading.present()
      .then(() => this.db.collection('users').add(user))
      .then(ref => {
        return loading.dismiss()
          .then(() => this.viewCtrl.dismiss(ref))
      })
  }

}

