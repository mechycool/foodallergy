import { Component } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';
import {UserProvider} from "../../providers/user";
import {Product} from "../../providers/api";

@IonicPage()
@Component({
  selector: 'page-result',
  templateUrl: 'result.html',
})
export class ResultPage {

  title: string;
  imgSrc: string;
  allergens: string[];

  set product (data: Product) {
    this.title = data.title;
    this.imgSrc = data.img || 'assets/imgs/product.png';
    // this.imgSrc = 'assets/imgs/product.png'; // TODO remove
    this.allergens = data.allergens
      .filter(allergen => !this.user.tailoring || this.user.allergens[allergen]) // Non Tailored users will be shown all found allergens
    // TODO get Better Products
  }


  constructor (
    private params: NavParams,
    private user: UserProvider
  ) {
    this.product = this.params.get('product');
  }

  setTestGroup() {

  }


}
