import {Component} from '@angular/core';
import {UserProvider} from "../../providers/user";
import {ActionSheetController, AlertController, Loading, LoadingController, ModalController} from "ionic-angular";
import {BarcodeScanner, BarcodeScanResult} from "@ionic-native/barcode-scanner";
import {ApiProvider, Product} from "../../providers/api";

@Component({
  templateUrl: 'home.html'
})
export class HomePage {

  testMode = false;

  constructor(
    private user: UserProvider,
    private barcode: BarcodeScanner,
    private api: ApiProvider,
    private modal: ModalController,
    private alert: AlertController,
    private loadingCtrl: LoadingController,
    private actionSheet: ActionSheetController,
  ) {
  }

  ionViewWillEnter () {
    // if (this.user.ref) this.scan();
  }

  signUp() {
    return this.user.createUser()
      .then(() => this.scan())
  }

  scan() {
    // TODO check Access to camera
    let scanner;
    if (this.testMode) {
      scanner = new Promise(resolve => {
        return this.actionSheet.create({
          title: 'Set Intensity of Tailoring',
          buttons: [
            {
              text: 'None',
              handler: () => resolve(0)
            },
            {
              text: 'Binary',
              handler: () => resolve(1)
            },
            {
              text: 'Full',
              handler: () => resolve(2)
            },
          ]
        }).present();
      })
        .then((tailoring: 0|1|2) => {
          this.user.tailoring = tailoring;
          return {
            format: 'EAN_13',
            text: '7610200017819',
            cancelled: false
          }
        });
    } else {
      scanner = this.barcode.scan();
    }
    return scanner
      .then((data: BarcodeScanResult) => {
        if (!data.cancelled) {
          const loading: Loading = this.loadingCtrl.create();
          loading.present()
            .then(() => this.api.fetchProduct(parseInt(data.text)))
            .then(product => this.displayResult(product))
            .catch(err => this.scanError(err))
            .then(() => loading.dismiss())
        }
      })
  }

  displayResult(product: Product) {
    return this.modal.create("ResultPage",{product, testMode: this.testMode}).present();
  }

  scanError(err: string) {
    switch (err) {
      case 'notFound':
        return this.alert.create({
          title: 'Product not found', // TODO i18n
          buttons: [{text: 'Nevermind'}] // TODO i18n
        }).present();
      default:
        return console.log(err)
    }
  }
}


// TODO Check Network Access
