import {NgModule, ErrorHandler} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicStorageModule } from "@ionic/storage";
import {HttpClientModule} from "@angular/common/http";
import {BarcodeScanner} from "@ionic-native/barcode-scanner";


import { AngularFireModule } from 'angularfire2';
import {AngularFirestore} from "angularfire2/firestore";
import { UserProvider } from '../providers/user';
import { ApiProvider } from '../providers/api';


export const firebaseConfig = {
  apiKey: "AIzaSyBydlknfi8cUgCUU5XAT9Zfo9KJgBpIvPc",
  authDomain: "foodallergy-dfec8.firebaseapp.com",
  databaseURL: "https://foodallergy-dfec8.firebaseio.com",
  projectId: "foodallergy-dfec8",
  storageBucket: "foodallergy-dfec8.appspot.com",
  messagingSenderId: "324907969469"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp,{mode:'md'}),
    AngularFireModule.initializeApp(firebaseConfig),
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    BarcodeScanner,
    AngularFirestore,
    UserProvider,
    ApiProvider
  ]
})
export class AppModule {}


