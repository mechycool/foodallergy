import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import {UserProvider} from "../providers/user";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  rootPage:any;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    user: UserProvider
  ) {
    Promise.all([
      // user.getFromStorage(),
      platform.ready()
    ])
      .then(() => {
        if (platform.is('cordova')) {
          statusBar.hide();
          splashScreen.hide();
        }
        this.rootPage = HomePage;
      });
  }
}


interface Allergens {
  [name:string]: 'caution'|'danger';
}
